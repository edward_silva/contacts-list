import React from 'react';
import { StyledList, StyledListItem, StyledImg, StyledListItemContent } from './style';

const ListViewContacts = ({ dataSource }) => {

    const isDataSourceEmpty = dataSource.length === 0;
    return (
        <StyledList>
            {
                !isDataSourceEmpty ?
                    dataSource.map(contact => (
                        <StyledListItem>
                            <StyledListItemContent>
                                <StyledImg src={contact.picture.thumbnail} />
                                <div>{contact.name.title}. {contact.name.last}, {contact.name.first}</div>
                            </StyledListItemContent>
                        </StyledListItem>
                    )) :
                    <span>{
                        'No results found'
                    }</span>
            }

        </StyledList>
    )
}

export default ListViewContacts;