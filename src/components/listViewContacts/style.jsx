import React from 'react';
import styled from 'styled-components';

export const StyledList = styled.ul`
    list-style:none;
    display:inline-grid;
    padding:0;
`;

export const StyledListItem = styled.li`
    display:inline-flex;
    margin-bottom:10px;
    
`;

export const StyledImg = styled.img`
    margin-right:20px;
`;

export const StyledListItemContent = styled.div`
    display:inline-flex;
    align-items:center;
`;

