import axios from 'axios';
import React, { useState, useEffect } from 'react';
import ListViewContacts from '../listViewContacts/listViewContacts';
import { StyledContainer } from './style';

const contactsBaseUrl = "https://randomuser.me/api/?results=30&inc=name,picture,email&nat=br";

const Home = () => {

    const [contactsList, setContactsList] = useState([]);
    // const [filteredContactsList, setFilteredContactsList] = useState([]);
    const [searchValue, setSearchValue] = useState("")

    // function filterDataSource(e) {
    //     const value = e.target.value;
    //     setFilteredContactsList(
    //         contactsList
    //             .filter(c => c.name.first.toUpperCase().includes(value) ||
    //                 c.name.last.toUpperCase().includes(value) ||
    //                 c.name.title.toUpperCase().includes(value))
    //     )
    // }

    useEffect(() => {
        axios.get(contactsBaseUrl)
            .then((response) => {
                if (response) {
                    setContactsList(response.data.results);
                    // setFilteredContactsList(response.data.results);
                }
            })
    }, []);

    return (
        <StyledContainer>
            <input value={searchValue} type="text" placeholder='Filter' onChange={e => setSearchValue(e.target.value)} />
            <ListViewContacts dataSource={
                contactsList
                    .filter(c =>
                        // c.name.first.toUpperCase().includes(searchValue.toUpperCase()) ||
                        // c.name.last.toUpperCase().includes(searchValue.toUpperCase()) ||
                        // c.name.title.toUpperCase().includes(searchValue.toUpperCase()) ||
                        `${c.name.title}. ${c.name.last}, ${c.name.first}`.trim().toUpperCase().includes(searchValue.trim().toUpperCase())
                    )
            } />
        </StyledContainer>
    )
}

export default Home;