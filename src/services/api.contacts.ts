import axios, { AxiosResponse } from 'axios';
import httpClient from './api.core'

const contactsBaseUrl = "https://randomuser.me/api/?results=30&inc=name,picture,email&nat=br";

class ApiContacts{

    getContacts():Promise<AxiosResponse<any,any>>{
        return httpClient.get(contactsBaseUrl);
    }
}

export default new ApiContacts();

